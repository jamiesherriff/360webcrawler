/* Crawler Module for 360 Assignment
 * Date last modified: 15 October 2014
 * Author: Jamie Sherriff
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <pthread.h>
#include "html.h"
#include "http.h"
#include "url.h"
#include "list.h"
#include "queue.h"

#define URL_SIZE 256

typedef struct PageStruct Page;
typedef struct WorkThreadStruct Work_thread;

typedef struct PageStruct {
    char* path;
    char* host;
    int depth;
    char* header;
    char* content;
    int content_len;
    int exit;
} Page;

typedef struct WorkThreadStruct {
    Queue* receive_queue;
    Queue* send_queue;
} Work_thread;



//  url_filename:
//  Convert a url into a filename for saving to local disk.
//  e.g. http://www.cosc.canterbury.ac.nz/dept/viscom.shtml  
// ->  www.cosc.canterbury.ac.nz/dept|viscom.shtml
void url_filename(char *buffer, int size, char *url) {
    char path[URL_SIZE];
    char host[URL_SIZE];
    get_path(path, URL_SIZE, url);
    get_host(host, URL_SIZE, url);
    if (*path == '\0') {
        strncpy(path, "/", URL_SIZE);
    }
    char* c = path;
    while (*c != '\0') {
        if (*c == '/') {
            *c = '|';
        }
        ++c;
    }
    snprintf(buffer, size, "%s/%s", host, path);
}

/*
 *initialises a page struct with information and pushes the page struct onto
 *the url_list
 */
void add_url(List* url_list, char* host, char* path, int depth) {
    Page* page = (Page*) malloc(sizeof (Page));
    char* path_buf = malloc(sizeof (char)* URL_SIZE);
    strncpy(path_buf, path, URL_SIZE);
    page->path = path_buf;
    page->host = host;
    page->depth = depth;
    page->exit = 0;
    push_start(url_list, page);
}


/*kills all working threads by sending a page struct with exit set
 * Then waits for the threads to exit
 */

void kill_threads(int num_workers, Queue* process_to_thread_queue, 
     pthread_t* thread) {
    int i = 0;
    Page* kill = (Page*) malloc(sizeof (Page));
    kill->exit = 1;
    for (i = 0; i < num_workers; ++i) {
        queue_put(process_to_thread_queue, kill);
    }
    i = 0;
    for (i = 0; i < num_workers; ++i) {
        pthread_join(thread[i], NULL);
    }
    free(kill);
}

/*Downloads the url in the Page* struct given and returns the ptr page struct
 * returns NULL on failure
 */

Page* download_page(Page* page) {
    if (page != NULL) {
        char host_buffer[URL_SIZE] = {'\0'};
        char path_buffer[URL_SIZE] = {'\0'};
        get_host(host_buffer, URL_SIZE, page->host);
        get_path(path_buffer, URL_SIZE, page->path);
        char* response = NULL;
        int content_len = http_query(host_buffer, path_buffer, 80, &response);
        if (response != NULL) {
            char *content = http_split_content(response);
            if (content == NULL) {
                printf("found null in content\n");
                return NULL;
            }
            page->content_len = content_len;
            page->header = response;
            page->content = content;
            // Decrease depth
            page->depth = page->depth - 1;
            return page;
        }
    }
    return NULL;
}


/*creates a directory, returns 0 if the directory already exists or if 
 * it sucesfully created it or -1 on failure.
 * NOT THREAD SAFE - due to stat checking and mkdir then creating directory
 */

int create_directory(char* dir_name) {
    int return_code;
    struct stat *stat_struct;
    stat_struct = malloc(sizeof (struct stat));
    return_code = stat(dir_name, stat_struct);
    if (return_code == 0) { // directory exists
        printf("directory %s already exists\n", dir_name);
    } else {
        if (errno == ENOENT) { // directory does not exist
            return_code = mkdir(dir_name, S_IRWXU);
            if (return_code != 0) { // if directory creation failed
                perror("mkdir failed");
                return_code = -1;
            }
        }
    }
    free(stat_struct);
    return return_code;
}

/*
 *frees all malloc'd page struct resources
 */

void free_page(Page * page) {
    free(page->path);
    free(page->header);
    free(page->content);
    free(page);
}

/*does a final memory clean up of malloc'd resources
 */

void clean_up(Queue* process_to_thread_queue, Queue* thread_to_process_queue, 
    List* url_list, Work_thread* worker_args) {
    free(worker_args);
    list_free(url_list);
    queue_free(process_to_thread_queue);
    queue_free(thread_to_process_queue);
}


/*gets all links in a given page struct and returns 0 if depth has been reach
 *returns the counter of the number of links found
 */

int get_links(Page* current_url, List* url_list, char* url) {
    int counter = 0;
    if (current_url->depth < 1) {
        return 0;
    }
    // extract links from inital url 
    char** links = extract_links(current_url->content);
    char** linkptr = links;
    while (*links) {
        char buffer_link[URL_SIZE];
        //Returns -1 if either URL is invalid, 1 for equal, 0 not equal.
        if ((make_absolute(buffer_link, URL_SIZE, url, *links)) == -1) {
            printf("Fail abosolute in main process\n");
            printf("Host = %s, path = %s\n", url, current_url->path);
        }
        int match = match_hosts(url, buffer_link);
        if (match == 1) {
            char file_name[URL_SIZE] = {'\0'};
            url_filename(file_name, strlen(buffer_link) + 1, buffer_link);
            int fd = open(file_name, O_CREAT | O_WRONLY | O_EXCL, S_IRUSR | S_IWUSR);
            if (fd < 0) {
                if (errno == EEXIST) {
                    // Pass no need to do anything if the file exists
                    //printf("File %s already exists, not adding to url list\n", file_name);
                }
            } else {
                add_url(url_list, url, *links, current_url->depth);
                counter++;
                close(fd);
            }
        }
        links++;
    }
    // reverts ptr to beginning of links so able to free correctly
    links = linkptr;
    free_strings(links);
    return counter;
}


/*writes the content to file only if its a valid 200 ok response
 *creates an empty file if another response status is given
 */

int write_file(Page* current_url, char* file_name) {
    int page_count = 0;
    FILE *fp;
    if ((fp = fopen(file_name, "wb")) == NULL) {
        fprintf(stderr, "Couldn't open file, file name = %s\n", file_name);
        exit(1);
    };
    if (strstr(current_url->header, "200 OK")) {
        // Write a valid 200 OK content to file
        printf("Saving page %s\n", current_url->path);
        fwrite(current_url->content, current_url->content_len, 1, fp);
        page_count += 1;
    }
    fclose(fp);
    return page_count;
}

/*
 *Worker thread that goes out and downloads a page and puts a page struct
 * onto the send_queue if successful
 */

void work_thread(Work_thread* args) {
    Queue* send_queue = args->receive_queue; // thread to proccess queue
    Queue* receive_queue = args->send_queue; //proccess to thread queue
    Page* current_url = queue_get(receive_queue);
    while (current_url && current_url->exit != 1) {
        Page* html = download_page(current_url);
        queue_put(send_queue, html);
        // Will block and wait for input
        current_url = queue_get(receive_queue);
    }
}

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "usage: ./crawler url depth num_workers\n");
        exit(1);
    }
    char url[URL_SIZE];
    add_scheme(url, URL_SIZE, argv[1]);
    // get host - e.g. www.canterbury.ac.nz
    char host[URL_SIZE];
    get_host(host, URL_SIZE, url);
    int depth = atoi(argv[2]);
    int num_workers = atoi(argv[3]);
    int page_count = 0;
    // Added error check for no depth or no worker threads
    if (depth < 1 || num_workers < 1) {
        fprintf(stderr, "Must have > 1 depth and workers\n");
        exit(1);
    }
    printf("Crawling %s to depth %d, with %d worker threads\n", url, depth, num_workers);
    // Queue to send single urls to a worker to crawl
    Queue* process_to_thread_queue = queue_alloc(num_workers);
    // Queue to recieve multiple urls back from a worker to add to url list
    Queue* thread_to_process_queue = queue_alloc(num_workers);
    // List of urls that only the initial process will access
    List* url_list = list_new();
    int list_counter = 0;
    // Begin with a base URL that you select, and push it on the top of your list
    char path_buffer[URL_SIZE] = {'\0'};
    get_path(path_buffer, URL_SIZE, url);
    add_url(url_list, url, path_buffer, depth);
    list_counter++;
    // Create base directory, exits if creation fails as cant make base directory
    if ((create_directory(host)) == -1) {
        list_free(url_list);
            // Clean up malloc'd variables
        queue_free(process_to_thread_queue);
        queue_free(thread_to_process_queue);
        fprintf(stderr, "Base directory creation directory name = %s\n", host);
        exit(1);
    }
    int i = 0;
    // Create a stuct of args so you can pass x amount of args to a thread as specified in struct
    Work_thread* worker_args = (Work_thread*) malloc(sizeof (Work_thread));
    worker_args->receive_queue = thread_to_process_queue;
    worker_args->send_queue = process_to_thread_queue;
    // spawn worker threads to crawl queue of urls
    pthread_t thread[num_workers];
    for (i = 0; i < num_workers; ++i) {
        pthread_create(&thread[i], NULL, (void*) work_thread, (void*) worker_args);
    }
    int thread_queue_count = 0;
    Page* input_url = pop_back(url_list);
    list_counter--;
    Page* current_url;
    queue_put(process_to_thread_queue, input_url);
    thread_queue_count++;
    while (list_counter > 0 || thread_queue_count > 0) {
        //add items up to the queue size which is num_workers
        while (list_counter > 0 && thread_queue_count < num_workers) {
            input_url = pop_back(url_list);
            list_counter--;
            queue_put(process_to_thread_queue, input_url);
            thread_queue_count++;
        }
        if (thread_queue_count > 0) {
            current_url = queue_get(thread_to_process_queue);
            thread_queue_count--;
            char file_name[URL_SIZE] = {'\0'};
            char buffer_file[URL_SIZE] = {'\0'};
            if ((make_absolute(buffer_file, URL_SIZE, url, 
                    current_url->path)) == -1) {
                printf("Fail absolute in main\n");
                printf("Host = %s, path = %s\n", url, current_url->path);
            }
            url_filename(file_name, strlen(buffer_file) + 1, buffer_file);
            page_count += write_file(current_url, file_name);
            list_counter += get_links(current_url, url_list, url);
            free_page(current_url);
        }
    }
    //calls the function to kill of all work threads
    kill_threads(num_workers, process_to_thread_queue, thread);
    //Clean up everything and free it
    clean_up(process_to_thread_queue, thread_to_process_queue, url_list, worker_args);
    printf("Valid Pages Downloaded: %d\n", page_count);
    return 0;
}

/*Crawler Design comments:
 *http.c and its header file has been modified to handle binary data so PDF's,
 * pictures can be downloaded.
 *
 * Crawler Performance analysis:
 *Running crawler with the command ./crawler www.cosc.canterbury.ac.nz 4 
 * [threads] gives the following time responses. Also note due to how unstable
 * writing performance varies on the cosc lab machines reproducing these exact
 * times may vary. The optimal thread count seems to be around 70 threads. The
 * length of the concurrent queues are the size of the number of worker threads
 * Increases in the queue size does not seem to have any impact on performance.
 * Any performance gains/losses are hard to determine due to the nature of
 * the cosc file system.
 * 
 * Times were recorded by using the unix command time and taken from the real
 *  time record.
 * 1 thread: 23.5s
 * 4 threads: 19.6s
 * 50 threads: 14.9s
 * 70 threads: 12.3s
 * 100 threads: 13.1s
 * 500 threads: 20.2s
 */