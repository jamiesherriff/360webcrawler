/* Queue Module for 360 Assignment
 * Date last modified: 15 October 2014
 * Author: Jamie Sherriff
 */
#include "queue.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

typedef struct LinkStruct Link;

/*
 * Queue - the abstract type of a concurrent queue.
 * You must provide an implementation of this type but it is
hidden from the outside.
 *
 */


typedef struct LinkStruct {
    void *data;
    Link *next;

} Link;

typedef struct QueueStruct {
    Link* head;
    Link* tail;
    sem_t read;
    sem_t write;
    pthread_mutex_t mutex;

} Queue;

/*
 * queue_alloc:
 *
 * Allocate a concurrent queue of a specific fixed size.
 *
 */


Queue *queue_alloc(int size) {
    Queue * queue = (Queue*) malloc(sizeof (Queue));
    queue->head = NULL;
    queue->tail = NULL;
    sem_init(&queue->read, 0, 0);
    sem_init(&queue->write, 0, size);
    pthread_mutex_t mutex_init = PTHREAD_MUTEX_INITIALIZER;
    queue->mutex = mutex_init;
    // sem_init(&queue->write, 0, 1);
    return queue;
}

/*
 * queue_free:
 *
 * Free a concurrent queue and associated memory.
 * NOTE: A user should not free a queue until any
 * users are finished. Any calling queue_free() while
 * any consumer/producer is waiting on queue_put or queue_get
 * will cause queue_free to print an error and exit the program.
 *
 * Assume the person knows how and when to free the queue so no checks are made
 */


void queue_free(Queue *queue) {
    free(queue);
}

/*
 * queue_put:
 *
 * Place an item into the concurrent queue.
 *
 * If there is no space available, queue_put will
 * block until a space becomes available when it will
 * put the item into the queue and immediately return.
 *
 * Uses void* to hold an arbitrary type of item,
 * it is the users responsibility to manage memory
 * and ensure it is correctly typed.
 *
 */

void queue_put(Queue *queue, void *item) {
    sem_wait(&queue->write);
    pthread_mutex_lock(&queue->mutex);
    Link *link = (Link*) malloc(sizeof (Link));
    link->data = item;

    // if queue is empty, head and tail = new link
    if (!queue->head) {
        queue->head = queue->tail = link;
        link->next = NULL;

    } else { // if not empty tail becomes new link, tails next points to new tail link
        queue->tail->next = link;
        queue->tail = link;
    }
    pthread_mutex_unlock(&queue->mutex);
    sem_post(&queue->read);
}

/*
 * queue_get:
 *
 * Get an item from the concurrent queue.
 *
 * If there is no item available then queue_get
 * will block until an item becomes avaible when
 * it will immediately return that item.
 *
 */

void *queue_get(Queue *queue) {
    sem_wait(&queue->read);
    pthread_mutex_lock(&queue->mutex);
    Link *link = queue->head;
    // if queue is empty
    if (link == NULL) {
        return NULL;
    }

    // if queue only has one item in it.
    if (queue->head == queue->tail) {
        queue->head = queue->tail = NULL;
    } else {
        queue->head = queue->head->next;
    }
    void *data = link->data;
    free(link);
    pthread_mutex_unlock(&queue->mutex);
    sem_post(&queue->write);
    return data;
}