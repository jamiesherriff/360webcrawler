/* http Module for 360 Assignment
 * Date last modified: 15 October 2014
 * Author: Jamie Sherriff
 */
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <netinet/in.h>
#include <sys/socket.h>


#define BUF_SIZE 1024
#define SOCKET_ERROR -1


/*
 *Creates a connection based on the given host and port
 * Will cause a exit failure if failure occurs 
 * returns a socket descriptor on success
 */
int create_connection(char *host, int port) {
    int socket_fd = -1;
    struct addrinfo *server_address = NULL;
    struct addrinfo hints;
    char port_num[32];
    //used to stop buffer overflow and convert port to string for getaddrinfo method
    snprintf(port_num, sizeof (port_num) - 1, "%d", port);

    /* Create a socket */
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd == SOCKET_ERROR) {
        perror("Failed to create socket");
        exit(EXIT_FAILURE);
    }
    /* Prepare hints. These help getaddrinfo to select the correct address */
    memset(&hints, 0, sizeof (struct addrinfo));
    hints.ai_family = AF_INET; 
    hints.ai_socktype = SOCK_STREAM;
    int ret_val = getaddrinfo(host, port_num, &hints, &server_address);
    if (ret_val < 0) {
        fprintf(stderr, "Could not resolve address\n");
        exit(1);
    }
    connect(socket_fd, server_address->ai_addr, server_address->ai_addrlen);
    freeaddrinfo(server_address);
    return socket_fd;
}

 /*
 * http_query:
 *
 * Perform an HTTP 1.0 query to a given host and page and port
number.
 * host is a hostname and page is a path on the remote server.
 *
 * On any error (for example an invalid path or host name)
 * NULL is returned and any resources associated with the query
 * are cleaned up. An error may be printed on stderr.
 *
 * The string returned is the raw http response with content,
 * and the user is responsible for freeing the memory.
 *
 * arguments:
 *
host - hostname e.g. www.canterbury.ac.nz
 *
page - e.g. /index.html
 */
int http_query(char *host, char *page, int port, char** buffer_ptr) {
    char* buffer = malloc(sizeof (char) * BUF_SIZE);
    int socket_fd = create_connection(host, port);
    char httpget[BUF_SIZE];
    //check as some links double up on slashes in the get request
    if (page[0] == '/') {
        //move pointer
        page++;
    }
    snprintf(httpget, BUF_SIZE - 1, "GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n", page, host);
    if (send(socket_fd, httpget, strlen(httpget), 0) < 0) {
        printf("something went wrong with the send\n");
        return -1;
    }
    int message_length = 0, total_received = 0, remain_buff = 0;
    /* Read results back from server */
    while ((message_length = read(socket_fd, &buffer[total_received], BUF_SIZE - 1)) > 0) {
        total_received += message_length;
        remain_buff = (malloc_usable_size(buffer));
        if (total_received + BUF_SIZE > remain_buff) {
            buffer = realloc(buffer, (total_received * 2) + sizeof (char) * BUF_SIZE);
        }
    }
    // Make sure that it is null terminated
    buffer[total_received] = '\0';
    char* content = strstr(buffer, "\r\n\r\n");
    if (content == NULL) {
        printf("Can not find content/header split\n");
        return -1;
    }
    int position = (content + 4) - buffer;
    int content_len = total_received - position;
    close(socket_fd); // close socket
    *buffer_ptr = buffer; //set arg pointer to local buffer
    return content_len;
}

// split http content from the response string
char* http_split_content(char *response) {
    // finds the first occurance of "\r\n\r\n" which is the break 
    // beteween header and content
    char* content = strstr(response, "\r\n\r\n");
    if (content == NULL) {
        printf("NULL FOUND IN HTTP_SPLIT_CONTENT\n");
        return NULL;
    }
    content += 4;
    // pointer subtraction to find where content starts
    int position = content - response;
    //returns number of bytes available in malloc'd buffer ptr
    int num_bytes = malloc_usable_size(response);
    char* return_ptr = malloc(num_bytes - position);
    memcpy(return_ptr, content, num_bytes - position);
    // force a '\0' delimiter in response so only shows header not content aswel
    // + 2 to skip the first "\r\n"
    response[position + 2] = '\0';
    return return_ptr;
}



